package com.blooper.robot.cleaner.cmd;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.*;

public class PropsConfig {
    
    private static class SortedProperties extends Properties {
        private static final long serialVersionUID = 1L;

        public Enumeration<Object> keys() {
            Enumeration<Object> keysEnum = super.keys();
            Vector<Object> keyList = new Vector<Object>();

            while (keysEnum.hasMoreElements()) {
                keyList.add(keysEnum.nextElement());
            }

            Collections.sort(keyList, new Comparator<Object>() {
                @Override
                public int compare(Object o1, Object o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            });

            return keyList.elements();
        }
    }

    private PropsConfig() {
    }

    public static Properties load(InputStream is) throws Exception {
        Properties properties = new SortedProperties();
        properties.load(is);
        return properties;
    }
    
    public static Properties load(Reader reader) throws IOException {
        Properties properties = new SortedProperties();
        properties.load(reader);
        return properties;
    }

    public static CommandLine parseOptions(String[] args) throws Exception {
        // all options in enum
        Options options = new Options();
        for (Cli cli : Cli.values()) {
            options.addOption(newOption(cli));
        }

        CommandLineParser parser = new BasicParser();
        try {
            CommandLine commandLine = parser.parse(options, args);       
            return defaultActions(commandLine);
        } catch (MissingArgumentException e) {
            printHelp();
            return null;
        }
    }
    
    public static CommandLine defaultActions(CommandLine commandLine) throws Exception {
        if (commandLine.hasOption(Cli.HELP.getOpt()) || commandLine.getOptions().length == 0) {
            // print help
            printHelp();
            return null;
        }
        return commandLine;
    }

    private static void printHelp() {
        System.out.printf("Por favor, digite os comandos corretamente, não tem doc de ajuda.\n");
    }

    private static Option newOption(Cli cli) {
        return OptionBuilder.withLongOpt(cli.getLongOpt()).withDescription(cli.getDesc()).hasArg(cli.hasArg()).create(cli.getOpt());
    }
}
