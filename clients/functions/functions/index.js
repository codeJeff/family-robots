// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const admin = require('firebase-admin');

const pushDM = require('./push-chat-private');
const pushPost = require('./push-chat-group');
const pushPostComments = require('./push-comments');
const pushBluepIsHidden = require('./push-inform-group-hidden');
const pushPostLikes = require('./push-likes');
const updateUploadStatus = require('./update-upload-status');
const linkedinAuth = require('./linkedin-auth');
const util = require('./util');

// The Firebase Admin SDK to access the Firebase Realtime Database. 
var serviceAccount = require("./service-account-key.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: functions.config().firebase.databaseURL
});

//const admin = require('firebase-admin');
//admin.initializeApp(functions.config().firebase);

// Pushs
exports.pushDM = pushDM.sendPushPrivate(functions, admin);
exports.pushPost = pushPost.sendPushGroup(functions, admin);
exports.pushPostComments = pushPostComments.sendPushComment(functions, admin);
exports.pushBluepIsHidden = pushBluepIsHidden.sendPushInformGroupIsHidden(functions, admin);
exports.pushPostLikes = pushPostLikes.sendPushLikes(functions, admin);

// Update message upload status
exports.updateUploadStatus = updateUploadStatus.updateUploadStatus(functions, admin);

// Login
// Linkedin Auth
exports.linkedinAuthToken = linkedinAuth.token(functions, admin);
exports.linkedinAuthRedirect = linkedinAuth.redirect(functions, admin);