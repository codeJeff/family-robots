const util = require('./util');

//
// SEND PUSH MESSAGE TO GROUP
//
module.exports = {
  
  sendPushGroup: function(firebaseFunctions, firebaseAdmin) {
    return firebaseFunctions.database.ref('/chats_core/chats_messages/{chatKey}/{key}').onWrite(event => {
      
      // Only edit data when it is first created.
      if (event.data.previous.exists()) {
        return;
      }
      
      // Exit when the data is deleted.
      if (!event.data.exists()) {
        return;
      }
      
      // Grab the current value of what was written to the Realtime Database.
      const original = event.data.val();
      
      var snapshot = event.data;
      const fromUserName = snapshot.child('userName').val();
      const thumbnail = snapshot.child('userThumbnail').val();
      const msgThumbnail = snapshot.child('msgThumbnail').val();
      const message = snapshot.child('message').val();
      const chatKey = snapshot.child('chatKey').val();
      const chatName = snapshot.child('chatName').val();
      const fromUUID = snapshot.child('uuid').val();
      const type = 'CHAT_GROUP';
      
      let payload = {
        data : {
          title: chatName,
          fromUserName: fromUserName,
          message: fromUserName + ': ' + message,
          chatKey : chatKey,
          chatName : chatName,
          userThumbnail : thumbnail || "",
          msgThumbnail : msgThumbnail || "",
          type : type,
          tag : chatKey,
          click_action : 'http://app.blueper.me'
        }
      };
      
      console.log('Payload:', event.params.key, payload);
      
      return getChatMembers(firebaseAdmin, chatKey).then(members => {
        
        return getUsers(firebaseAdmin, members).then(users => {
          let tokens = [];
          for (let user of users) {
            const token = user['firebaseToken'];
            const uuid = user['uuid'];
            if(!util.isEmpty(token) && uuid != fromUUID) {
              console.log('uuid | token:', uuid, token);
              tokens.push(token);
            }
          }
          return firebaseAdmin.messaging().sendToDevice(tokens, payload);
        });
        
      });
    });
  }
}

function getChatMembers(firebaseAdmin, chatKey) {
  let dbRef = firebaseAdmin.database().ref('/chats_core/chat_users/'+chatKey);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      let users = [];
      for (var uuid in data) {
        users.push(data[uuid]);
      }
      resolve(users);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}

function getUsers(firebaseAdmin, members) {
  let promise = new Promise((resolve, reject) => {
    var promises = [];
    let users = [];
    for (let member of members) {
      const uuid = member['uuid'];
      promises.push(getUser(firebaseAdmin, uuid).then(user => {
        return user;
      }));
    }

    Promise.all(promises).then(function(data) {
      resolve(data);
    }).catch((e) => {
      console.log("Error to merge promisses afte getUsers", e);
    });
  }, (err) => {
    reject(err);
  });
  return promise;
}

function getUser(firebaseAdmin, uuid) {
  let dbRef = firebaseAdmin.database().ref('/users/'+uuid);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      resolve(data);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}