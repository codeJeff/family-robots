const util = require('./util');

//
// SEND PUSH MESSAGE TO USERS INSIDE A FINISHED GROUP
//
module.exports = {

  sendPushInformGroupIsHidden: function(firebaseFunctions, firebaseAdmin) {
    return firebaseFunctions.database.ref('/chats_core/chats/{key}').onUpdate(event => {

             // Grab the current value of what was written to the Realtime Database.
             const original = event.data.val();

             var snapshot = event.data;
        
             const isVisible = snapshot.child('isVisible').val();  
             if(isVisible) {
              return;
             } 

             const chatKey = snapshot.child('key').val();
             const chatName = snapshot.child('name').val();             

             const title = 'O bluep '+chatName+' já fechou.';
             const message = 'Crie um bluep onde você está agora.';
            
                let payload = {
                    data : {
                      title: title,
                      message: message,
                    }
                };

                console.log('Payload:', event.params.key, payload);

                return getChatMembersFCMToken(firebaseAdmin, chatKey).then(users => {
                  let tokens = [];
                  for (let user of users) {
                    const token = user['firebaseToken'];
                    const uuid = user['uuid'];
                    if(!util.isEmpty(token)) {
                      console.log('token:', chatKey, token);
                      tokens.push(token);
                    }
                  }
                 return firebaseAdmin.messaging().sendToDevice(tokens, payload);
          });

      });
  }
}

function getChatMembersFCMToken(firebaseAdmin, chatKey) {
  let dbRef = firebaseAdmin.database().ref('/chats_core/chat_users/'+chatKey);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      let users = [];
      for (var uuid in data) {
        users.push(data[uuid]);
      }
      resolve(users);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}