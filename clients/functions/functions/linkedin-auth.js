const util = require('./util');
const cookieParser = require('cookie-parser');
const crypto = require('crypto');
const OAUTH_SCOPES = ['r_basicprofile', 'r_emailaddress'];
const cors = require('cors')({
  origin: true,
});
const axios = require('axios');

/**
* Creates a configured LinkedIn API Client instance.
*/
function linkedInClient(firebaseFunctions) {
  // LinkedIn OAuth 2 setup
  // TODO: Configure the `linkedin.client_id` and `linkedin.client_secret` Google Cloud environment variables.
  return require('node-linkedin')(
    firebaseFunctions.config().linkedin.client_id,
    firebaseFunctions.config().linkedin.client_secret,
    'https://blueper-2287b.firebaseapp.com/login-linkedin');
  }
  
  /**
  * Creates a Firebase account with the given user profile and returns a custom auth token allowing
  * signing-in this account.
  * Also saves the accessToken to the datastore at /linkedInAccessToken/$uid
  *
  * @returns {Promise<string>} The Firebase custom auth token in a promise.
  */
  function createFirebaseAccount(linkedinID, displayName, photoURL, email, description, accessToken, firebaseAdmin) {
    // Invalid user id
    if(util.isEmpty(linkedinID)) {
      throw error;
      return
    }
    
    // The UID we'll assign to the user.
    const uid = 'linkedin:'+linkedinID;
    
    // Save the access token tot he Firebase Realtime Database.
    const databaseTaskUUID = firebaseAdmin.database().ref('/users/'+ uid +'/uuid').set(uid);
    const databaseTaskDisplayName = firebaseAdmin.database().ref('/users/'+ uid +'/name').set(displayName);
    const databaseTaskThumbnailUrl = firebaseAdmin.database().ref('/users/'+ uid +'/thumbnailUrl').set(photoURL);
    const databaseTaskEmail = firebaseAdmin.database().ref('/users/'+ uid +'/email').set(email);
    const databaseTaskDescription = firebaseAdmin.database().ref('/users/'+ uid +'/description').set(description);

    const databaseTaskAccessToken = firebaseAdmin.database().ref('/users/'+ uid +'/firebaseToken').set(accessToken);

    // Create or update the user account.
    const userCreationTask = firebaseAdmin.auth().updateUser(uid, {
      displayName: displayName,
      photoURL: photoURL,
      email: email,
      emailVerified: true,
    }).catch((error) => {
      // If user does not exists we create it.
      if (error.code === 'auth/user-not-found') {
        return firebaseAdmin.auth().createUser({
          uid: uid,
          displayName: displayName,
          photoURL: photoURL,
          email: email,
          emailVerified: true,
        });
      } 
      throw error;
    });
    
    // Wait for all async task to complete then generate and return a custom auth token.
    return Promise.all([userCreationTask, 
      databaseTaskAccessToken, 
      databaseTaskDisplayName,
      databaseTaskEmail,
      databaseTaskThumbnailUrl,
      databaseTaskUUID,
      databaseTaskDescription
    ]).then(() => {
      // Create a Firebase custom auth token.
      return firebaseAdmin.auth().createCustomToken(uid);
    }).then((token) => {
      console.log('Created Custom token for UID "', uid, '" Token:', token);
      return token;
    });
  }
  
  // Receive linkedin token and create user and return firebase token
  function generateUserFromLinkedinToken(req, res, firebaseFunctions, firebaseAdmin) {
    const linkedinClient = linkedInClient(firebaseFunctions);
    
    try {
      return cookieParser()(req, res, () => {
        if (!req.query.state) {
          throw new Error('State cookie not set or expired. Maybe you took too long to authorize. Please try again.');
        }
        console.log('Received verification state:', req.query.state);
        linkedinClient.auth.authorize(OAUTH_SCOPES, req.query.state); // Makes sure the state parameter is set
        console.log('Received auth code:', req.query.code);
        console.log('Received state:', req.query.state);
        linkedinClient.auth.getAccessToken(res, req.query.code, req.query.state, (error, results) => {
          if (error) {
            throw error;
          }
          console.log('Received Access Token:', results.access_token);
          const linkedinRequest = linkedinClient.init(results.access_token);
          linkedinRequest.people.me((error, userResults) => {
            if (error) {
              throw error;
            }
            console.log('Auth code exchange result received:', userResults);
            
            // We have a LinkedIn access token and the user identity now.
            const accessToken = results.access_token;
            const linkedInUserID = userResults.id;
            const profilePic = userResults.pictureUrl;
            const userName = userResults.formattedName;
            const email = userResults.emailAddress;
            const description = userResults.headline;
            
            // Create a Firebase account and get the Custom Auth Token.
            return createFirebaseAccount(linkedInUserID, userName, profilePic, email, description, accessToken, firebaseAdmin).then(
              (firebaseToken) => {
                // [START sendResponse]
                console.log('Sending token:', firebaseToken);
                res.status(200).send(firebaseToken);
                // [END sendResponse]
              });
            });
          });
        });
      } catch (error) {
        res.status(200).send(error);
      }
    }
    
    //
    // SEND PUSH MESSAGE TO GROUP
    //
    module.exports = {
      
      /**
      * Redirects the User to the LinkedIn authentication consent screen. ALso the 'state' cookie is set for later state
      * verification.
      */
      redirect: function(firebaseFunctions, firebaseAdmin) {
        return firebaseFunctions.https.onRequest((req, res) => {
          
          const linkedinClient = linkedInClient(firebaseFunctions);
          
          cookieParser()(req, res, () => {
            const state = req.cookies.state || crypto.randomBytes(20).toString('hex');
            console.log('Setting verification state:', state);
            res.cookie('state', state.toString(), {
              maxAge: 3600000,
              secure: true,
              httpOnly: true,
            });
            linkedinClient.auth.authorize(res, OAUTH_SCOPES, state.toString());
          });
        });
      },
      
      /**
      * Exchanges a given LinkedIn auth code passed in the 'code' URL query parameter for a Firebase auth token.
      * The request also needs to specify a 'state' query parameter which will be checked against the 'state' cookie.
      * The Firebase custom auth token is sent back in a JSONP callback function with function name defined by the
      * 'callback' query parameter.
      */
      token: function(firebaseFunctions, firebaseAdmin) {
        return firebaseFunctions.https.onRequest((req, res) => {
          return cors(req, res, () => {
            return generateUserFromLinkedinToken(req, res, firebaseFunctions, firebaseAdmin);
          });
        });
      }
      
    }