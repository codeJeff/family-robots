package com.blooper.robot.cleaner.useCases;

import com.blooper.robot.cleaner.model.DateDifference;
import com.blooper.robot.cleaner.model.Group;
import com.blooper.robot.cleaner.util.DateDifferenceUtil;
import com.google.firebase.database.*;
import com.google.firebase.tasks.OnCompleteListener;
import com.google.firebase.tasks.Task;

import java.util.*;
import java.util.concurrent.CountDownLatch;

public class HideGroups extends FirebaseParams implements Runnable {

    private List<Group> groupsToRemove = new ArrayList<>();

    public HideGroups(CountDownLatch latch) {
        super(latch);
    }

    @Override
    public void run() {
        try {
            requestNodes();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("\nError to run hideGroups (latch.await()) ");
        }
    }

    public void requestNodes() throws InterruptedException {
        Query query = databaseReference.child(KEY_CHATS_CORE_CHATS).orderByChild(KEY_IS_VISIBLE).equalTo(STATUS_VISIBLE);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Group> groups = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Group group = snapshot.getValue(Group.class);
                    groups.add(group);
                }

                verifyRuleToHide(groups);

                if (groupsToRemove.isEmpty()) {
                    System.out.println("\nNão tem grupo para ser ocultado no momento, tente mais tarde.\n");

                    killRobotProcess();
                }

                hideGroups();

            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.out.println("error : " + error.getMessage());
                latch.countDown();
            }
        });
    }

    private void verifyRuleToHide(List<Group> groups) {
        for (Group group : groups) {
            Date chatDate = new Date(group.getCreationDate());
            Date now = new Date();
            DateDifference dateDifference = new DateDifferenceUtil().getDateDifferenceToText(chatDate, now);

            if (dateDifference.getDays() > 0) { // This group has more then 24 hours
                groupsToRemove.add(group); // This group won be deleted
            }
        }
    }

    private void hideGroups() {
        for (final Group group : groupsToRemove) {
            if (group.getKey().isEmpty() || group.getKey() == null) {
                return;
            }

            DatabaseReference ref = databaseReference.child(KEY_CHATS_CORE_CHATS).child(group.getKey()).child(KEY_IS_VISIBLE);
            ref.setValue(STATUS_HIDE).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(Task<Void> task) {
                    System.out.println("name [" + group.getName() + "] | key [" + group.getKey() + "] : ocultado.\n");

                    groupsToRemove.remove(group);

                    if (groupsToRemove.isEmpty()) {
                        killRobotProcess();
                    }

                }
            });
        }
    }

    private void killRobotProcess() {
        System.out.println("\nTchau Rosie!\n");

        latch.countDown();
    }

}
