package com.blooper.robot.cleaner.useCases;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.CountDownLatch;

/**
 * Created by jefferson.silva on 25/09/17.
 */
public class FirebaseParams {

    protected static final String KEY_CHATS_CORE_CHATS = "/chats_core/chats";

    protected static final boolean STATUS_VISIBLE = true;
    protected static final boolean STATUS_HIDE = false;

    protected static final String KEY_IS_VISIBLE = "isVisible";

    protected final DatabaseReference databaseReference;

    protected final CountDownLatch latch;

    public FirebaseParams(CountDownLatch latch) {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        this.latch = latch;
    }
}
