const util = require('./util');

//
// SEND PUSH MESSAGE WHEN USER LIKE A POST IN TIMELINE
//
module.exports = {
  
  sendPushLikes: function(firebaseFunctions, firebaseAdmin) {
    return firebaseFunctions.database.ref('/chats_core/chats_messages_likes/{chatKey}/{chatMessageKey}/{key}').onWrite(event => {
      
      // Only edit data when it is first created.
      if (event.data.previous.exists()) {
        return;
      }
      
      // Exit when the data is deleted.
      if (!event.data.exists()) {
        return;
      }
      
      // Grab the current value of what was written to the Realtime Database.
      let original = event.data.val();
      
      var snapshot = event.data;
      let chatKey = snapshot.child('chatKey').val();
      let chatMessageKey = snapshot.child('chatMessageKey').val();
      let sourceMessageKey = snapshot.child('chatMessageKey').val();
      let userLikedPostUUID = snapshot.child('uuid').val();
      let type = 'CHAT_GROUP';   

      // GET SOURCE MESSAGE TEXT
      return getChatMessage(firebaseAdmin, chatKey, chatMessageKey).then(message => {
        let chatName = message['chatName'];
        let messageText = message['message'];
        let messageThumbnail = message['msgThumbnail'];
        let userCreatedPostUUID = message['uuid'];
        
        // GET CHAT MEMBERS FIREBASE_TOKEN
        return getUser(firebaseAdmin, userLikedPostUUID).then(userLikedPost => {
          let userLikedName = userLikedPost['name'];
          let userLikedThumbnail = userLikedPost['thumbnailUrl'];

          return getUser(firebaseAdmin, userCreatedPostUUID).then(userCreatedPost => {
            let token = userCreatedPost['firebaseToken'];
            let tokens = [];
            
            let payload = {
              data : {
                title: userLikedName + ' curtiu seu post',
                fromUserName: userLikedName + ' curtiu seu post',
                message: messageText,
                chatKey : chatKey,
                chatName : chatName,
                userThumbnail : userLikedThumbnail,
                msgThumbnail : messageThumbnail,
                sourceMessageKey : sourceMessageKey,
                type : type
              }
            };  
            
            // Dont send push to generate event user
            if(!util.isEmpty(token)) {
              console.log('token:', chatKey, token);
              tokens.push(token);
            }  
            
            console.log('Payload:', event.params.key, payload);
            
            return firebaseAdmin.messaging().sendToDevice(tokens, payload);
          });
        });
      });
    });
  }
}

function getUser(firebaseAdmin, fromUUID) {
  let dbRef = firebaseAdmin.database().ref('/users/'+fromUUID);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      resolve(data);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}

function getChatMessage(firebaseAdmin, chatKey, chatMessageKey) {
  let dbRef = firebaseAdmin.database().ref('/chats_core/chats_messages/'+chatKey+'/'+chatMessageKey);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      resolve(data);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}