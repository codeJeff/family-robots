const util = require('./util');

//
// SEND PUSH MESSAGE TO USER IN A CHAT PRIVATE
//
module.exports = {

  sendPushPrivate: function(firebaseFunctions, firebaseAdmin) {
    return firebaseFunctions.database.ref('/chats_private_core/chats_messages/{chatKey}/{key}').onWrite(event => {

            // Only edit data when it is first created.
            if (event.data.previous.exists()) {
             return;
           }

            // Exit when the data is deleted.
            if (!event.data.exists()) {
              return;
            }

             // Grab the current value of what was written to the Realtime Database.
             const original = event.data.val();

             var snapshot = event.data;
             const fromUserName = snapshot.child('userName').val();
             const thumbnail = snapshot.child('userThumbnail').val();
             const msgThumbnail = snapshot.child('msgThumbnail').val();
             const message = snapshot.child('message').val();
             const chatKey = snapshot.child('chatKey').val();
             const toUUID = snapshot.child('toUUID').val();
             const type = 'CHAT_PRIVATE';

             let payload = {
              data : {
                fromUserName: fromUserName,
                message: message,
                chatKey : chatKey,
                userThumbnail : thumbnail,
                msgThumbnail : msgThumbnail,
                type : type,
                toUUID : toUUID
              }
            };

            console.log('Payload:', event.params.key, payload);

            return getUserFCMToken(firebaseAdmin, toUUID).then(user => {
              let tokens = [];
              const token = user['firebaseToken'];
              const uuid = user['uuid'];
              if(!util.isEmpty(token)) {
                console.log('token:', uuid, token);
                tokens.push(token);
              }
              return firebaseAdmin.messaging().sendToDevice(tokens, payload);
            });

          });
  }
}

function getUserFCMToken(firebaseAdmin, uuid) {
  let dbRef = firebaseAdmin.database().ref('/users/'+uuid);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      resolve(data);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}