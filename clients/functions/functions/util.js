
//
// SEND PUSH MESSAGE TO USER IN A CHAT PRIVATE
//
module.exports = {

  isEmpty: function(value) {
      return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
  }
  
}
