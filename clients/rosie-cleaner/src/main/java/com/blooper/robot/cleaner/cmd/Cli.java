package com.blooper.robot.cleaner.cmd;

public enum Cli {
    HELP("h", "help", "Mostra os codigos de ajuda", false),

    // processing
    HIDE_GROUPS("hn", "hide_nodes", "Deixa invisivel os chats com mais de 24 horas", false),

    // processing
    PRINT_GROUPS("pn", "print_nodes", "Mostra todos os chats e ha quanto tempo foram criados ou quanto tempo resta", false);

    private String opt;
    private String longOpt;
    private String desc;
    private boolean hasArg;

    private Cli(String opt, String longOpt, String desc, boolean hasArg) {
        this.opt = opt;
        this.longOpt = longOpt;
        this.desc = desc;
        this.hasArg = hasArg;
    }

    public String getOpt() {
        return opt;
    }

    public String getLongOpt() {
        return longOpt;
    }

    public String getDesc() {
        return desc;
    }

    public boolean hasArg() {
        return hasArg;
    }
}