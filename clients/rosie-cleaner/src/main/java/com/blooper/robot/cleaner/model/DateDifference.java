package com.blooper.robot.cleaner.model;

/**
 * Created by jefferson.silva on 05/07/17.
 */
public class DateDifference {

    public long days;
    public long hours;
    public long minutes;
    public long seconds;

    public DateDifference(long elapsedDays, long elapsedHours, long elapsedMinutes, long elapsedSeconds) {
        days = elapsedDays;
        hours = elapsedHours;
        minutes = elapsedMinutes;
        seconds = elapsedSeconds;
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public long getHours() {
        return hours;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }

    public long getMinutes() {
        return minutes;
    }

    public void setMinutes(long minutes) {
        this.minutes = minutes;
    }

    public long getSeconds() {
        return seconds;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }
}
