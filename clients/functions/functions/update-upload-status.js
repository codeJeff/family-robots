const util = require('./util');

//
// SEND PUSH MESSAGE TO GROUP
//
module.exports = {
  
  updateUploadStatus: function(firebaseFunctions, firebaseAdmin) {
    return firebaseFunctions.database.ref('/chats_core/chats_messages/{chatKey}/{key}').onUpdate(event => {
      
      // Exit when the data is deleted.
      if (!event.data.exists()) {
        return;
      }
      
      // Grab the current value of what was written to the Realtime Database.
      const original = event.data.val();
      
      var snapshot = event.data;
      let key = snapshot.child('key').val();
      
      // Contents
      const msgThumbnail = snapshot.child('msgThumbnail').val();
      const videoUrl = snapshot.child('video').child('videoUrl').val();
      const message = snapshot.child('message').val();
      
      // Type
      const typeText = snapshot.child('typeText').val();
      
      // Status
      var statusText = "UPLOADING";
      
      if(typeText == 'IMAGE' && msgThumbnail) {
        console.log('msgThumbnail:', key, msgThumbnail);
        statusText = "DONE"
      }
      
      if(typeText == 'VIDEO' && videoUrl) {
        console.log('videoUrl:', key, videoUrl);
        statusText = "DONE"
      }
      
      if(typeText == 'TEXT' && message) {
        console.log('message:', key, message);
        statusText = "DONE"
      }
      
      console.log('statusText:', key, statusText);
      
      // You must return a Promise when performing asynchronous tasks inside a Functions such as
      // writing to the Firebase Realtime Database.
      return event.data.ref.child('statusText').set(statusText);
    });
  }
  
}