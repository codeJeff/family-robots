package com.blooper.robot.cleaner.main;

import com.blooper.robot.cleaner.cmd.Cli;
import com.blooper.robot.cleaner.cmd.PropsConfig;
import com.blooper.robot.cleaner.ext.FirebaseAuthRobot;
import com.blooper.robot.cleaner.useCases.HideGroups;
import com.blooper.robot.cleaner.useCases.PrintNodes;
import org.apache.commons.cli.CommandLine;

import java.util.concurrent.CountDownLatch;

public class RosieMain {


    public static void main(String[] args) throws Exception {
        startFirebase();

        receiveCommands(args);

        System.exit(0);
    }

    private static void startFirebase() {
        FirebaseAuthRobot.newInstance();
    }

    private static void receiveCommands(String[] args) throws Exception {
        CommandLine commandLine = PropsConfig.parseOptions(args);

        if (commandLine == null) {
            System.exit(0);
        }

        // cleaner nodes
        if (commandLine.hasOption(Cli.HIDE_GROUPS.getOpt())) {
            hideGroups();
        }

        // print nodes
        if (commandLine.hasOption(Cli.PRINT_GROUPS.getOpt())) {
            printGroups();
        }
    }

    private static void hideGroups() {
        CountDownLatch latch = new CountDownLatch(1);
        System.out.printf("cml : %s\n", Cli.HIDE_GROUPS.getLongOpt());
        hideGroups(latch);
    }

    private static void printGroups() {
        CountDownLatch latch = new CountDownLatch(1);
        System.out.printf("cml : %s\n", Cli.PRINT_GROUPS.getLongOpt());
        printGroups(latch);
    }

    private static void printGroups(CountDownLatch latch) {
        System.out.println("\n:: printGroups ::\n");

        PrintNodes printNodes = new PrintNodes(latch);

        try {
            new Thread(printNodes).start();
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("\nError to run printGroups (latch.await()) ");
        }
    }

    private static void hideGroups(CountDownLatch latch) {
        System.out.println("\n:: hideGroups ::\n");

        HideGroups hideGroups = new HideGroups(latch);

        try {
            new Thread(hideGroups).start();
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("\nError to run hideGroups (latch.await()) ");
        }
    }

}