const util = require('./util');

//
// SEND PUSH MESSAGE TO GROUP
//
module.exports = {
  
  sendPushComment: function(firebaseFunctions, firebaseAdmin) {
    return firebaseFunctions.database.ref('/chats_core/chats_messages_comments/{chatKey}/{sourceMessageKey}/{key}').onWrite(event => {
      
      // Only edit data when it is first created.
      if (event.data.previous.exists()) {
        return;
      }
      
      // Exit when the data is deleted.
      if (!event.data.exists()) {
        return;
      }
      
      // Grab the current value of what was written to the Realtime Database.
      const original = event.data.val();
      
      var snapshot = event.data;
      const fromUserName = snapshot.child('userName').val() + ' comentou';
      const thumbnail = snapshot.child('userThumbnail').val();
      const msgThumbnail = snapshot.child('msgThumbnail').val();
      const message = snapshot.child('message').val();
      const chatKey = snapshot.child('chatKey').val();
      const chatName = snapshot.child('chatName').val();
      const fromUUID = snapshot.child('uuid').val();
      const sourceMessageKey = snapshot.child('sourceMessageKey').val();
      const type = 'CHAT_GROUP';
      
      // GET SOURCE MESSAGE TEXT
      return getChatMessage(firebaseAdmin, chatKey, sourceMessageKey).then(sourceMessage => {
        let sourMessageText = sourceMessage['message'];
        let sourMessageThumbnail = sourceMessage['msgThumbnail'];
        
        // Post message is empty
        if(util.isEmpty(sourMessageText) || sourMessageText == ""){
          // Push title is chatName now
          sourMessageText = chatName + ':';
        }
        
        if(util.isEmpty(sourMessageText)){
          sourMessageText = chatName;
        }
        
        let payload = {
          data : {
            title: sourMessageText,
            fromUserName: fromUserName,
            message: fromUserName + ': ' + message,
            chatKey : chatKey,
            chatName : chatName,
            userThumbnail : thumbnail || "",
            msgThumbnail : msgThumbnail || "",
            sourceMessageKey : sourceMessageKey,
            type : type
          }
        };
        
        console.log('Payload:', event.params.key, payload);
        
        // GET CHAT MEMBERS FIREBASE_TOKEN
        return getChatMembers(firebaseAdmin, chatKey).then(users => {
          let tokens = [];
          for (let user of users) {
            const token = user['firebaseToken'];
            const uuid = user['uuid'];
            if(!util.isEmpty(token) && uuid != fromUUID) {
              console.log('token:', chatKey, token);
              tokens.push(token);
            }
          }
          
          return firebaseAdmin.messaging().sendToDevice(tokens, payload);
        });
      });
    });
  }
}

function getChatMembers(firebaseAdmin, chatKey) {
  let dbRef = firebaseAdmin.database().ref('/chats_core/chat_users/'+chatKey);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      let users = [];
      for (var uuid in data) {
        users.push(data[uuid]);
      }
      resolve(users);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}

function getChatMessage(firebaseAdmin, chatKey, sourceMessageKey) {
  let dbRef = firebaseAdmin.database().ref('/chats_core/chats_messages/'+chatKey+'/'+sourceMessageKey);
  let defer = new Promise((resolve, reject) => {
    dbRef.once('value', (snap) => {
      let data = snap.val();
      resolve(data);
    }, (err) => {
      reject(err);
    });
  });
  return defer;
}