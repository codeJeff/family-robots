package com.blooper.robot.cleaner.util;

import com.blooper.robot.cleaner.model.DateDifference;

import java.util.Date;

/**
 * Created by jefferson.silva on 05/07/17.
 */
public class DateDifferenceUtil {

    /**
     * Get the difference between two dates in days, hours, minutes and seconds
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public DateDifference getDateDifferenceToText(Date startDate, Date endDate) {
        // milliseconds
        long different = Math.abs(endDate.getTime() - startDate.getTime());

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return new DateDifference(elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
    }

}
