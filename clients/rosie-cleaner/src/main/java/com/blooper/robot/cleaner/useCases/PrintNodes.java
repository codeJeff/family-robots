package com.blooper.robot.cleaner.useCases;

import com.blooper.robot.cleaner.model.Group;
import com.blooper.robot.cleaner.model.DateDifference;
import com.blooper.robot.cleaner.util.DateDifferenceUtil;
import com.google.firebase.database.*;

import java.util.Date;
import java.util.concurrent.CountDownLatch;

/**
 * Created by jefferson.silva on 05/07/17.
 */
public class PrintNodes implements Runnable {

    private final CountDownLatch latch;

    public PrintNodes(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        requestNodes();
    }

    public void requestNodes() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("/chats_core/chats");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Group group = snapshot.getValue(Group.class);
                    printChatDateInfo(group);
                }
                latch.countDown();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.out.println("error : " + error.getMessage());
                latch.countDown();
            }
        });
    }

    private void printChatDateInfo(final Group group) {
        System.out.println("group[" + group.name + "]");

        Date chatDate = new Date(group.creationDate);
        Date now = new Date();
        DateDifference dateDifference = new DateDifferenceUtil().getDateDifferenceToText(chatDate, now);

        if (dateDifference.getDays() > 0) {

            if (dateDifference.getDays() == 1) {
                System.out.println(" Criado ha " + dateDifference.getDays() + " dia.");
            } else {
                System.out.println(" Criado ha " + dateDifference.getDays() + " dias.");
            }

            if(group.isVisible) {
                System.out.println(" !SERÁ OCULTADO!\n");
            } else {
                System.out.println(" !OCULTO!\n");
            }

        } else if (dateDifference.getHours() > 0) {
            System.out.println(" Criado ha " + dateDifference.getHours() + " horas e " + dateDifference.getMinutes() + " minutos.");

            long timeToClear = (dateDifference.getHours() - 24) * -1;

            if (timeToClear == 1) {
                System.out.println(" Ainda resta " + timeToClear + " hora.\n");
            } else {
                System.out.println(" Ainda restam " + timeToClear + " horas.\n");
            }
        }
    }

}
