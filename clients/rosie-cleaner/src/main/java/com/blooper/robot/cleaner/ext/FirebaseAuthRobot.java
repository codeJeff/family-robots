package com.blooper.robot.cleaner.ext;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FirebaseAuthRobot {

    // server file path
    public static final String MAC_FILE_PATH = "/robots_account/firebase_service_account.json";

    // windows file path
    public static final String WINDOWS_FILE_PATH = "c:/robots_account/firebase_service_account.json";

    // locauz server file path
    public static final String LOCAUZ_SERVER_FILE_PATH = "/family_robots/firebase_service_account.json";

    private final String[] paths = {MAC_FILE_PATH, WINDOWS_FILE_PATH, LOCAUZ_SERVER_FILE_PATH};

    private static final String FIREBASE_DATABASE = "https://blueper-2287b.firebaseio.com/";

    public static FirebaseAuthRobot instance;

    public static FirebaseAuthRobot newInstance() {
        if (instance == null) {
            instance = new FirebaseAuthRobot();
            instance.buildAuth();
        }
        return instance;
    }

    private void buildAuth() {
        File file = getFirebaseAccountFile();

        try {
            FileInputStream serviceAccount = new FileInputStream(file);

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredential(FirebaseCredentials.fromCertificate(serviceAccount))
                    .setDatabaseUrl(FIREBASE_DATABASE)
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getFirebaseAccountFile() {
        File file = null;
        for (String path : paths) {
            File testFile = new File(path);
            if (testFile.exists()) {
                file = testFile;
                break;
            }
        }
        return file;
    }


}